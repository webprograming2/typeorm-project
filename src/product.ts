import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productsRepository = AppDataSource.getRepository(Product)

    const users = await productsRepository.find()
    console.log("Loaded products: ", users)

    const updatedProduct = await productsRepository.findOneBy({id: 2})
    console.log(updatedProduct)
    updatedProduct.price = 75
    await productsRepository.save(updatedProduct)

}).catch(error => console.log(error))
